# Name
k8s (l)log (u)ser (w)ebhook driven by golx

# Features
- Logs user info  on mutating operations

# Constraints 
- Logs only mutating operations 
- Does not log unauthorized operations

# src
Based on grumpy
https://docs.giantswarm.io/advanced/custom-admission-controller/

# go part
```
go build . #just to check it 
docker login
docker build -t maciekleks/luw-golx .
docker push maciekleks/luw-golx
```

# k8s deploying
Modify `certs/ca-config.txt` and `certs/luw-config.tx` first. Create server side (luw) certificates (CA and webhook certs):
```
./gen-certs.sh 
```
Modify `templates/_manifest.yml` template accordigly to your needs. Generate k8s manifest from the template:
```
./gen-manifest.sh
```
Create secret with the luw cerificate, e.g.: 
```
kubectl create secret generic luw-golx -n default \
  --from-file=key.pem=certs/luw-golx-key.pem \
  --from-file=cert.pem=certs/luw-golx-crt.pem
```
Apply the manifest:
```
kubectl apply -f manifest.yml
```