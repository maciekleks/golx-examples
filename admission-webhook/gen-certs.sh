#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

# CREATE THE PRIVATE KEY FOR OUR CUSTOM CA
openssl genrsa -out certs/ca.key 2048

# GENERATE A CA CERT WITH THE PRIVATE KEY
openssl req -new -x509 -key certs/ca.key -out certs/ca.crt -config certs/ca-config.txt

# CREATE THE PRIVATE KEY FOR OUR VOW SERVER
openssl genrsa -out certs/luw-golx-key.pem 2048

# CREATE A CSR FROM THE CONFIGURATION FILE AND OUR PRIVATE KEY
openssl req -new -key certs/luw-golx-key.pem -subj "/CN=luw-golx.default.svc" -out certs/luw-golx.csr -config certs/luw-golx-config.txt

# CREATE THE CERT SIGNING THE CSR WITH THE CA CREATED BEFORE
openssl x509 -req -in certs/luw-golx.csr \
	-CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -out certs/luw-golx-crt.pem \
	-extensions v3_req -extfile certs/luw-golx-config.txt
