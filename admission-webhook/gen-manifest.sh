#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

# INJECT CA IN THE WEBHOOK CONFIGURATION
export CA_BUNDLE=$(cat certs/ca.crt | base64 | tr -d '\n')
cat templates/_manifest.yml | envsubst >manifest.yml
