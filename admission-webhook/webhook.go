package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"os"
	"time"

	"github.com/rs/zerolog"
	sh "gitlab.com/maciekleks/golx/components/http"
	c "gitlab.com/maciekleks/golx/core"
	admissionV1 "k8s.io/api/admission/v1"
)

const (
	defaultPort = "8443"
)

var (
	tlscert, tlskey string
)

func init() {
	flag.StringVar(&tlscert, "tlsCertFile", "/etc/certs/cert.pem", "File containing the Webhook x509 Certificate in PEM format.")
	flag.StringVar(&tlskey, "tlsKeyFile", "/etc/certs/key.pem", "File containing the Webhook Private Key in PEM format.")
	flag.Parse()
}

func main() {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	logger := zerolog.New(output).With().Caller().Timestamp().Logger()

	routes := c.Routes{
		"/log": {
			From: &sh.SourceHttp{},
			To: func(cctx c.CoreContext, g *c.Gate) *c.Gate {
				return g.To(cctx, c.Transform, func(cctx c.CoreContext, e *c.Exchange) error {
					var body []byte

					if e.Body != nil {
						body = e.Body.([]byte)
					}
					if len(body) == 0 {
						cctx.Config.Logger.Error().Msg("empty body")
						return nil
					}
					arRequest := admissionV1.AdmissionReview{}
					cctx.Config.Logger.Info().Msgf("Request body: %s\n", string(body))
					if err := json.Unmarshal(body, &arRequest); err != nil {
						cctx.Config.Logger.Error().Msg("incorrect body")
						return c.WrapError(nil, "incorrect body error")
					}

					e.Body = arRequest.Request.UserInfo

					cctx.Config.Logger.Info().Msgf("userInfo: %+v\n", arRequest.Request.UserInfo)
					return nil

				}).To(cctx, c.Log)
			},
		},
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	cert, err := tls.LoadX509KeyPair(tlscert, tlskey)
	if err != nil {
		logger.Error().Msgf("Filed to load key pair: %v", err)
	}

	logger.Info().Msgf("golx routes listening on port: %s", port)
	routes.Serve(":"+port,
		c.WithLogger(&logger),
		c.WithTLS(&tls.Config{
			Certificates: []tls.Certificate{cert},
		},
		))
}
