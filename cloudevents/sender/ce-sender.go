package main

import (
	"encoding/json"
	//"errors"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog"
	sce "gitlab.com/maciekleks/golx/components/cehttp"
	st "gitlab.com/maciekleks/golx/components/timer"
	c "gitlab.com/maciekleks/golx/core"
	duckv1 "knative.dev/pkg/apis/duck/v1"
	"os"
	"time"
)

type envConfig struct {
	// Sink URL where to send cloudevents
	Sink string `envconfig:"K_SINK"`
	// CEOverrides are the CloudEvents overrides to be applied to the outbound event.
	CEOverrides string `envconfig:"K_CE_OVERRIDES"`
	// Name of this pod.
	Name string `envconfig:"POD_NAME"`
	// Namespace this pod exists in.
	Namespace string `envconfig:"POD_NAMESPACE"`
	// Whether to run continuously or exit.
	OneShot bool `envconfig:"ONE_SHOT" default:"false"`
}

type Timerbeat struct {
	Sequence int    `json:"id"`
	Label    string `json:"label"`
}

var (
	eventSource string
	sink        string
)

func main() {

	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	logger := zerolog.New(output).With().Timestamp().Logger()

	var env envConfig
	if err := envconfig.Process("", &env); err != nil {
		logger.Fatal().Msgf("[ERROR] Failed to process env var: %s", err)
	}

	if env.Sink != "" {
		sink = env.Sink
	}

	var ceOverrides *duckv1.CloudEventOverrides
	if len(env.CEOverrides) > 0 {
		overrides := duckv1.CloudEventOverrides{}
		err := json.Unmarshal([]byte(env.CEOverrides), &overrides)
		if err != nil {
			logger.Fatal().Msgf("[ERROR] Unparseable CloudEvents overrides %s: %v", env.CEOverrides, err)
		}
		ceOverrides = &overrides
	}

	tb := &Timerbeat{
		Sequence: 0,
		Label:    "golx-label",
	}

	routes := c.Routes{
		"/timer": {
			From: &st.SourceTimer{10 * time.Second},
			To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
				return pin.
					To(cctx, c.Transform, func(cctx c.CoreContext, e *c.Exchange) error {
						event := cloudevents.NewEvent(cloudevents.VersionV1)

						event.SetType("golx-test-event")
						eventSource = env.Namespace + "/" + env.Name
						event.SetSource(eventSource)

						if ceOverrides != nil && ceOverrides.Extensions != nil {
							for n, v := range ceOverrides.Extensions {
								event.SetExtension(n, v)
							}
						}

						if err := event.SetData(cloudevents.ApplicationJSON, tb); err != nil {
							cctx.Config.Logger.Error().Msgf("failed to set cloudevents data: %s", err.Error())
						}

						e.BodyType = "cloudevent"
						e.Body = event

						/*if env.OneShot {
							return c.WrapError(errors.New("OneShot"), "One shot interruption")
						}*/

						return nil
					}).
					To(cctx, c.Log).
					ToP(cctx, sce.CloudEventsHttp,
						c.ApplyOptions(
							sce.WithUrl(sink),
							sce.WithRetryParams(&sce.CloudEventsRetryParams{
								Strategy: sce.BackoffStrategyLinear,
								Period:   10 * time.Millisecond,
								MaxTries: 10,
							})))
			}, //To
		}, //route
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	logger.Info().Msgf("golx routes listening on port: %s", port)
	routes.Serve(":"+port, c.WithLogger(&logger))
}
