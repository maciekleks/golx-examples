module example.golx/cloudevents/sender

go 1.15

require (
	github.com/cloudevents/sdk-go/v2 v2.3.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/rs/zerolog v1.20.0
	gitlab.com/maciekleks/golx v0.1.3
	knative.dev/pkg v0.0.0-20210115202020-5bb97df49b44
)
