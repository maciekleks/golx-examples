This example shows how to use Timer component.

# Prerequisites
Update dependencies first
```bash
go get -u
```

## Standalone run
```bash
go run timer.go
```

## Docker build and push
Enter the directory
```bash
cd examples/timer
```
Build docker, e.g.
```bash
docker build -t YOUR_REPO_USERNAME/timer .
```
Push docker image to you repo, e.g.
```bash
docker push YOUR_REPO_USERNAME/timer
``` 


## knative - declarative way using kustomize
Run [Docker build and publish](#docker-build-and-push) first.

Customize image using your repo, e.g.
```bash
pushd k8s/knative/overlays/dev
kustomize edit set image yourImage=docker.io/YOUR_REPO_USERNAME/timer
kustomize edit set namespace golx #default namespace by default
popd
```
Run (you are non in examples/timer)
```bash
kubectl apply -k k8s/knative/overlays/dev
```

## knative - imperative way
Run [Docker build and publish](#docker-build-and-push) first.

```bash
kn service create timer --image=docker.io/YOUR_REPO_USERNAME/timer
```


