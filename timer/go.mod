module example.golx/timer

go 1.15

replace gitlab.com/maciekleks/golx => ../../../golx

require (
	github.com/prometheus/common v0.15.0 // indirect
	github.com/rs/zerolog v1.20.0
	gitlab.com/maciekleks/golx v0.0.0-00010101000000-000000000000
	//gitlab.com/maciekleks/golx v0.1.1
	golang.org/x/sys v0.0.0-20201214210602-f9fddec55a1e // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
