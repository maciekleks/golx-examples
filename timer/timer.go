package main

import (
	"github.com/rs/zerolog"
	sh "gitlab.com/maciekleks/golx/components/http"
	st "gitlab.com/maciekleks/golx/components/timer"
	c "gitlab.com/maciekleks/golx/core"
	"os"
	"time"
)

func main() {

	routes := c.Routes{
		"/timer": {
			From: &st.SourceTimer{10 * time.Second},
			To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
				return pin.To(cctx, c.Log)
			},
		},
		"/": {
			From: &sh.SourceHttp{},
			To: func(cctx c.CoreContext, g *c.Gate) *c.Gate {
				return g.To(cctx, c.Transform, func(cctx c.CoreContext, e *c.Exchange) error {
					in := string(e.Body.([]uint8))
					//transform by adding # at the end
					e.Body = in + "#"
					return nil
				})
			},
		},
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	logger := zerolog.New(output).With().Timestamp().Logger()
	logger.Info().Msgf("golx routes listening on port: %s", port)
	routes.Serve(":"+port, c.WithLogger(&logger))
}
